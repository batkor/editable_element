# Editable elements

### About
This module provides editable elements.

### Use

Use the form element as usual
```php
 $build['examples_link'] = [
    '#title' => $this->t('Examples'),
    '#type' => 'e_link',
    '#key' => 'example_key',
    '#url' => '/my_path',
  ];
```
Goto page and click to you new element. for change `title` and `url`

If you need custom element, you can create `@FormElement` and use `editable_element.manager` service.
See `\Drupal\editable_element\Element\EditableLink`
