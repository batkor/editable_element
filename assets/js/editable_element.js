(function (Drupal) {
  Drupal.behaviors.editableElement = {
    attach: function (context, settings) {
      context = context || document;
      context
        .querySelectorAll('.editable_element')
        .forEach(element => {
          if (element.processed) {
            return;
          }
          Drupal.ajax({
            progress: { type: 'throbber' },
            dialogType: 'dialog',
            dialogRenderer: 'off_canvas',
            dialog: {
              width: 500,
            },
            url: element.getAttribute('data-href'),
            event: 'click',
            element: element,
          })
          element.processed = true;
        })
    }
  }
}(Drupal))
