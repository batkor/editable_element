<?php

namespace Drupal\editable_element\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseDialogCommand;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The main editable_element form.
 */
class EditableElementForm extends FormBase {

  /**
   * The manager render elements.
   *
   * @var \Drupal\Core\Render\ElementInfoManager
   */
  protected $elementManager;

  /**
   * The editable element manager.
   *
   * @var \Drupal\editable_element\EditableElementManager
   */
  protected $editableElementManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = new static();
    $instance->elementManager = $container->get('plugin.manager.element_info');
    $instance->editableElementManager = $container->get('editable_element.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'editable_element_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, string $type = NULL, string $key = NULL) {
    if (empty($type) || empty($key)) {
      $this->messenger()->addError('Not found special keys for build form.');
      return $form;
    }
    /** @var \Drupal\editable_element\Element\EditableElementInterface $editableElementInstance */
    $editableElementInstance = $this
      ->elementManager
      ->createInstance($type);

    $form_state->set('ee_instance', $editableElementInstance);
    $form_state->set('ee_key', $key);

    $form['actions'] = [
      '#type' => 'actions',
      '#weight' => 50,
    ];

    $form['actions']['submit'] = [
      '#type' => 'button',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
      '#ajax' => [
        'callback' => '::submitForm',
      ],
    ];

    $form['actions']['cancel'] = [
      '#type' => 'submit',
      '#value' => $this->t('Cancel'),
      '#name' => 'close',
      '#ajax' => [
        'callback' => '::submitForm',
      ],
    ];

    $editableElementInstance->buildForm($form, $form_state, $this->editableElementManager);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    if ($form_state->getTriggeringElement()['#name'] !== 'close') {
      $form_state->get('ee_instance')->submitForm($form_state, $this->editableElementManager, $response);
      Cache::invalidateTags([$form_state->get('ee_key')]);
    }
    $response->addCommand(new CloseDialogCommand('#drupal-off-canvas'));
    return $response;
  }

}
