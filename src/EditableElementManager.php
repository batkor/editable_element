<?php

namespace Drupal\editable_element;

use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;

/**
 * The manager service for content editable elements..
 */
class EditableElementManager {

  /**
   * The collection mane in key value storage.
   */
  const COLLECTION = 'editable_element';

  /**
   * The key value store.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueFactoryInterface
   */
  protected $keyValueFactory;

  /**
   * The editable key value store.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected $store;

  /**
   * Account proxy service.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $accountProxy;

  /**
   * This permission status  for editable elements.
   *
   * @var bool
   */
  protected $allowed;

  /**
   * Constructs an EditableManager object.
   *
   * @param \Drupal\Core\KeyValueStore\KeyValueFactoryInterface $keyValueFactory
   *   The key value factory.
   */
  public function __construct(KeyValueFactoryInterface $keyValueFactory, AccountProxyInterface $accountProxy) {
    $this->keyValueFactory = $keyValueFactory;
    $this->accountProxy = $accountProxy;
  }

  /**
   * Attach handler for element.
   *
   * @param array $element
   *   The element structure.
   */
  public function attachHandler(array &$element) {
    // Skip if disallowed.
    if (!$this->allowed()) {
      return;
    }
    if (empty($element['#attributes']['title'])) {
      $element['#attributes']['title'] = new TranslatableMarkup('Click for edit item');
    }
    if (isset($element['#attributes']['class']) && is_string($element['#attributes']['class'])) {
      $element['#attributes']['class'] = [$element['#attributes']['class']];
    }
    $element['#attributes']['class'][] = 'editable_element';
    $element['#attributes']['data-href'] = Url::fromRoute('editable_element.form', [
      'type' => $element['#type'],
      'key' => $element['#key'],
    ])->toString();
    $element['#cache']['tags'][] = $element['#key'];
    $element['#attached']['library'][] = 'editable_element/main';
  }

  /**
   * Returns values from store and set default value.
   *
   * @param string $key
   *   The key in store.
   * @param mixed $default_value
   *   The default value if key not exists.
   *
   * @return mixed
   *   The value in store.
   */
  public function getValue($key, $default_value = NULL) {
    if ($value = $this->getStore()->get($key)) {
      return $value;
    }
    $this->setValue($key, $default_value);
    return $default_value;
  }

  /**
   * Set value in store.
   *
   * @param string $key
   *   The key.
   * @param mixed $value
   *   The value.
   */
  public function setValue($key, $value) {
    $this->getStore()->set($key, $value);
  }

  /**
   * Returns store for editable elements.
   *
   * @return \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   *   The key value store.
   */
  public function getStore() {
    if ($this->store) {
      return $this->store;
    }
    $this->store = $this->keyValueFactory->get(self::COLLECTION);
    return $this->store;
  }

  /**
   * Returns access status to editing editable elements.
   *
   * @return bool
   *   The access status.
   */
  protected function allowed() {
    if (is_null($this->allowed)) {
      $this->allowed = $this->accountProxy->hasPermission('access editable_element');
    }

    return $this->allowed;
  }

}
