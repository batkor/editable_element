<?php


namespace Drupal\editable_element\Element;


use Drupal\Core\Render\Element\HtmlTag;
use Drupal\editable_element\EditableElementManager;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a render element for fast editing link.
 *
 * Usage example:
 * @code
 * $build['examples_link'] = [
 *   '#title' => $this->t('Examples'),
 *   '#type' => 'e_link',
 *   '#key' => 'example_key',
 *   '#url' => '/my_path',
 * ];
 * @endcode
 *
 * @RenderElement("e_link")
 */
class EditableLink extends HtmlTag implements EditableElementInterface {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array &$form, FormStateInterface $form_state, EditableElementManager $manager) {
    $value = $manager->getValue($form_state->get('ee_key'));
    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#default_value' => $value['title'],
    ];
    $form['url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('URL'),
      '#default_value' => $value['url'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(FormStateInterface $form_state, EditableElementManager $manager, AjaxResponse $ajaxResponse) {
    $new_value = [
      'title' => $form_state->getValue('title'),
      'url' => $form_state->getValue('url'),
    ];
    $manager->setValue($form_state->get('ee_key'), $new_value);
    $selector = ".editable_element[data-href*='{$form_state->get('ee_key')}']";
    $ajaxResponse
      ->addCommand(new HtmlCommand($selector, $new_value['title']))
      ->addCommand(new InvokeCommand($selector, 'attr', ['href', $new_value['url']]));
  }

  /**
   * {@inheritdoc}
   */
  public static function preRenderHtmlTag($element) {
    if (!is_string($element['#url'])) {
      \Drupal::messenger()->addError('Key `#url` expects a string.');
    }
    /** @var \Drupal\editable_element\EditableElementManager $manager */
    $manager = \Drupal::service('editable_element.manager');
    $value = $manager->getValue($element['#key'], [
      'title' => $element['#title'],
      'url' => $element['#url'],
    ]);
    $element += ['#tag' => 'a'];
    $element['#value'] = $value['title'];
    $element['#attributes']['href'] = $value['url'];
    $manager->attachHandler($element);
    return parent::preRenderHtmlTag($element);
  }

}
