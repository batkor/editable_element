<?php

namespace Drupal\editable_element\Element;

use Drupal\editable_element\EditableElementManager;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Form\FormStateInterface;

interface EditableElementInterface {

  /**
   * The form constructor for special editable element.
   *
   * @param array $form
   *   The form structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param \Drupal\editable_element\EditableElementManager $manager
   *   The content editable manager.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array &$form, FormStateInterface $form_state, EditableElementManager $manager);

  /**
   * The save submit handler.
   *
   * Use '$form_state->get('ee_key')' for get key in store.
   * Use ' $form_state->get('ee_instance')' for get form element instance.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param \Drupal\editable_element\EditableElementManager $manager
   *   The content editable manager.
   */
  public function submitForm(FormStateInterface $form_state, EditableElementManager $manager, AjaxResponse $ajaxResponse);

}
