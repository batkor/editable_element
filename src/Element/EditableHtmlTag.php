<?php

namespace Drupal\editable_element\Element;

use Drupal\editable_element\EditableElementManager;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\HtmlTag;

/**
 * Provides a render element for fast editing html_tag.
 *
 * Properties:
 *   Key from HtmlTag form element @see \Drupal\Core\Render\Element\HtmlTag.
 *   - #key: The key for save in store.
 *
 * Usage example:
 * @code
 * $build['hello'] = [
 *   '#type' => 'e_html_tag',
 *   '#tag' => 'p',
 *   '#key' => 'example_key',
 *   '#value' => $this->t('Hello World'),
 * ];
 * @endcode
 *
 * @RenderElement("e_html_tag")
 */
class EditableHtmlTag extends HtmlTag implements EditableElementInterface {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array &$form, FormStateInterface $form_state, EditableElementManager $manager) {
    $form['value'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Value'),
      '#default_value' => $manager->getValue($form_state->get('ee_key')),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public static function preRenderHtmlTag($element) {
    /** @var \Drupal\editable_element\EditableElementManager $manager */
    $manager = \Drupal::service('editable_element.manager');
    $element['#value'] = $manager->getValue($element['#key'], $element['#value']);
    $manager->attachHandler($element);
    return parent::preRenderHtmlTag($element);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(FormStateInterface $form_state, EditableElementManager $manager, AjaxResponse $ajaxResponse) {
    $manager->setValue($form_state->get('ee_key'), $form_state->getValue('value'));
    $ajaxResponse->addCommand(new HtmlCommand(".editable_element[data-href*='{$form_state->get('ee_key')}']", $form_state->getValue('value')));
  }

}
